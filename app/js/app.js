'use strict';

/* App Module */

var hufApp = angular.module('huf', ['ngResource']);

hufApp.config(['$routeProvider', function($routeProvider) {
	$routeProvider.
/* 		Related Users Pages */
		when('/users', {templateUrl : 'partials/user-list.html', controller : UserListCtrl}).
		when('/users/:id', {templateUrl : 'partials/user-detail.html', controller : UserDetailCtrl}).
		when('/users/:id/questions', {templateUrl : 'partials/user-questions.html', controller : UserDetailCtrl}).
/* 		Related Questions Pages */
		when('/questions', {templateUrl : 'partials/question-list.html', controller : QuestionListCtrl}).
		when('/questions/:id', {templateUrl : 'partials/question-detail.html', controller : QuestionDetailCtrl}).
/* 		Log In Pages */
		when('/login', {templateUrl : 'partials/login.html', controller : LoginCtrl}).
		when('/signin', {templateUrl : 'partials/signin.html', controller : SigninCtrl}).
/* 		Asking Question Pages */
		when('/ask', {templateUrl : 'partials/ask.html', controller : askCtrl
		}).
/* 		Related Tags Pages */
		when('/tags', {templateUrl : 'partials/tag-list.html', controller : TagListCtrl}).
		when('/questions/tag/:tag', {templateUrl : 'partials/question-list.html', controller : TagQuestionListCtrl}).
/* 		Website Pages */
		when('/home', {templateUrl : 'partials/home.html', controller : HomeCtrl}).
		when('/error/:status', {templateUrl : 'partials/error.html', controller : ErrorCtrl}).
		otherwise({redirectTo: '/error/404'});
}]);
