'use strict';

/* Services */
var url = "http://localhost\\:8080/huf-api/rest";
var map ="/questions";
	/* User services */
hufApp.factory('Question', function($resource) {
	return $resource(url+'/questions', {}, {
		query: {method:'GET', isArray:true}
	});
});

hufApp.factory('Question', function($resource) {
	return $resource(url+'/questions/:id', {}, {
		get: {method:'GET', isArray:true}
	});
});

hufApp.factory('Question', function($resource) {
	return $resource(url+'/questions', {}, {
		save: {method:'POST'}
	});
});

hufApp.factory('Question', function($resource) {
	return $resource(url+'/questions/:id', {}, {
		remove: {method:'DELETE'}
	});
});


