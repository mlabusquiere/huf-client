'use strict';

var url = "http://localhost\\:8080/huf-api/rest/";
var map = "/login";

hufApp.factory('Login', function($resource) {
	return $resource(url+'/login', {}, {
		save: {method:'POST'}
	});
});