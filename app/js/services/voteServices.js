'use strict';

/* Services */
var url = "http://localhost\\:8080/huf-api/rest";
var map = "";
	/* Vote services */
// hufApp.factory('Vote', function($resource) {
// 	return $resource('http://localhost\\:8080/huf-api/rest/votes', {}, {
// 		query: {method:'GET', isArray:true}
// 	});
// });

hufApp.factory('Vote', function($resource) {
	return $resource(url+'/:type/:id/vote/', {}, {
		save: {method:'POST'}
	});
});

// hufApp.factory('Vote', function($resource) {
// 	return $resource('http://localhost\\:8080/huf-api/rest/votes/:id', {}, {
// 		get: {method:'GET', isArray:true}
// 	});
// });

// hufApp.factory('Vote', function($resource) {
// 	return $resource('http://localhost\\:8080/huf-api/rest/votes/:id', {}, {
// 		remove: {method:'DELETE'}
// 	});
// });

// hufApp.factory('Vote', function($resource) {
// 	return $resource('http://localhost\\:8080/huf-api/rest/votes/:id', {}, {
// 		update: {method:'PUT'}
// 	});
// });
