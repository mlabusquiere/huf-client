'use strict';

/* Services */
var url = "http://localhost\\:8080/huf-api/rest";
var map = "/tags";
	/* Tag services */
hufApp.factory('Tag', function($resource) {
	return $resource(url+'/tags', {}, {
		query: {method:'GET', isArray:true}
	});
});

hufApp.factory('Tag', function($resource) {
	return $resource(url+'/tags/:tag', {}, {
		get: {method:'GET', isArray:true}
	});
});

// hufApp.factory('Tag', function($resource) {
// 	return $resource('http://localhost\\:8080/huf-api/rest/tags/', {}, {
// 		save: {method:'POST'}
// 	});
// });

// hufApp.factory('Tag', function($resource) {
// 	return $resource('http://localhost\\:8080/huf-api/rest/tags/:id', {}, {
// 		get: {method:'GET', isArray:true}
// 	});
// });

// hufApp.factory('Tag', function($resource) {
// 	return $resource('http://localhost\\:8080/huf-api/rest/tags/:id', {}, {
// 		remove: {method:'DELETE'}
// 	});
// });

// hufApp.factory('Tag', function($resource) {
// 	return $resource('http://localhost\\:8080/huf-api/rest/tags/:id', {}, {
// 		update: {method:'PUT'}
// 	});
// });
