'use strict';

/* Services */
var url = "http://localhost\\:8080/huf-api/rest";
var map = "/answers";
	/* User services */
hufApp.factory('Answer', function($resource) {
	return $resource(url+'/answers', {}, {
		query: {method:'GET', isArray:true}
	});
});

hufApp.factory('Answer', function($resource) {
	return $resource(url+'/answers/:id', {}, {
		get: {method:'GET', isArray:true}
	});
});

hufApp.factory('Answer', function($resource) {
	return $resource(url+'/answers/:id', {}, {
		save: {method:'POST'}
	});
});

hufApp.factory('Answer', function($resource) {
	return $resource(url+'/answers/:id', {}, {
		remove: {method:'DELETE'}
	});
});


