'use strict';

/* Services */
var url = "http://localhost\\:8080/huf-api/rest";
	/* User services */
hufApp.factory('User', function($resource) {
	return $resource(url+'/users', {}, {
		query: {method:'GET', isArray:true}
	});
});

hufApp.factory('User', function($resource) {
	return $resource(url+'/users', {}, {
		save: {method:'POST'}
	});
});

hufApp.factory('User', function($resource) {
	return $resource(url+'/users/:id', {}, {
		get: {method:'GET', isArray:true}
	});
});

hufApp.factory('User', function($resource) {
	return $resource(url+'/users/:id', {}, {
		remove: {method:'DELETE'}
	});
});

// hufApp.factory('User', function($resource) {
// 	return $resource('http://localhost\\:8080/huf-api/rest/users/:id', {}, {
// 		update: {method:'PUT'}
// 	});
// });
