'use strict';

/* Question Detail Controller */

function QuestionDetailCtrl($scope, $location, $routeParams, $timeout, $http, Question, Answer, Vote) {

	$scope.newAnswer = {};
	$scope.newAnswer.text = "";

	$scope.formInfo = {};
	$scope.formInfo.text = "";

	$scope.question = {};
	Question.get({id: $routeParams.id}, 
		function(data) {
			$scope.question = data;
			console.log($scope.question);
		}, 
		function(error) {
			if(error.status === 404) {
				$location.path('/error/404');
			}
		}
	);
	
	$scope.addAnswer = function() {
		$scope.formInfo.text = "";
		if($scope.newAnswer === undefined || $scope.newAnswer.text === undefined || $scope.newAnswer.text === "") {
			if($scope.newAnswer.text === "" || $scope.newAnswer.text === undefined) {
				$scope.formInfo.text = "You have not answered";
			}
		}
		else {
			Answer.save({id: $routeParams.id}, $scope.newAnswer.text);
			console.info($scope.newAnswer);

			$scope.newAnswer = '';
			$timeout(function() {
				$scope.question = Question.get({id: $routeParams.id});
			}, 200);
		}
	};

	$scope.addVote = function(id, type, value) {

		var refPost = id;

		$http.post('http://localhost:8080/huf-api/rest/'+type+'/'+refPost+'/vote', value, {
			headers: {
				'Content-Type': 'application/json', //x-www-form-urlencoded
			}
		}).
		success(function(data, status, headers, config) {
			// console.info("Vote");
			// $('#myModalLabel').html("A Voter");
			// $('#myModalText').html("Votre vote a bien été pris en compte");
			// $('#myModalButton').hide();
			// $('#myModal').modal('show');
			// setTimeout(function() {$('#myModal').modal('hide');}, 1000);
			setTimeout(function() {
				Question.get({id: $routeParams.id}, 
					function(data) {
						$scope.question = data;
						console.log($scope.question);
					}, 
					function(err) {

					}
				);
			}, 0);
		}).
		error(function(data, status, headers, config){
			console.warn('Try again');
			$('#myModalLabel').html("Error");
			$('#myModalText').html("Il y a eu un problème, votre vote n'as pas été pris en compte");
			$('#myModalButton').hide();
			$('#myModal').modal('show');
			setTimeout(function() {$('#myModal').modal('hide');}, 3000);
		});

		// $timeout(function() {
		// 	$scope.question = Question.get({id: $routeParams.id});
		// }, 100);
	};

	$scope.getVotes = function(votes, positive) {
		if(votes == null) {
			return -1;
		}
		var nb = [0, 0];
		for (var i = votes.length - 1; i >= 0; i--) {
			console.log
			if(votes[i].coeff < 0) {
				nb[0]++;
			}
			nb[1] = votes.length - nb[0];
		};
		return nb[positive];	
	}
}

//QuestionDetailCtrl.$inject = ['$scope', '$location', '$routeParams', 'Question'];



// <form ng-submit="submit()" ng-controller="Ctrl">
//   <input type="submit" ng-click="setAtts('A', 'B')" value="Edit" />
//   <input type="submit" ng-click="setAtts('C', 'D')" value="Delete" />
// </form>

// <script>
//   function Ctrl($scope) {
//     $scope.submit = function() {
//       alert($scope.att1);
//       alert($scope.att2);
//     };

//     $scope.setAtts = function(a1, a2) {
//       $scope.att1 = a1;
//       $scope.att2 = a2;
//     };
//   }
// </script>
