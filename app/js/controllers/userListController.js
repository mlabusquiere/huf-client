'use strict';

/* User Controllers */

function UserListCtrl($scope, $location, User) {

	if($scope.logged === "true") {
		User.query(function(data) {
			$scope.users = data;
		}, function(error) {
			$location.path('/error/'+error.status);
		});
	}
	else {
		$location.path('/login');
	}
	
	$scope.deleteUser = function (id) {
		$scope.users = User.remove({id: id});
		$scope.refresh();
		$location.path('/users');
	};

}

// UserListCtrl.$inject = ['$scope', '$location', 'User'];
