'use strict';

/* User Detail Controller */

function UserDetailCtrl($scope, $location, $routeParams, User) {

	User.get({
		id: $routeParams.id
	}, function(data) {
		$scope.user = data;
	}, function(err) {
		// TODO
	});

}

//UserDetailCtrl.$inject = ['$scope', '$routeParams', 'User'];

