'use strict';

/* Controllers */

	/* User Controllers */

function ErrorCtrl($scope, $routeParams) {
	$scope.title = $scope.getStatus("title", $routeParams.status);
	$scope.text = $scope.getStatus("text", $routeParams.status);
}