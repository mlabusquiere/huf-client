'use strict';

/* Question List Controller */

function TagQuestionListCtrl($scope, $location, $routeParams, $filter, $timeout, Question, Tag) {

	Tag.get({
		tag: $routeParams.tag
	}, function(data) {
		$scope.questions = data;
		$scope.currentPage = 0;
		$scope.pageSize = 3;
		$scope.aroundPages = 2;
		
		$scope.groupedQuestions = [];

	}, function(error) {
		console.warn("Question.query() error : "+err);
		$('#myModalLabel').html("Error");
		$('#myModalText').html(error);
		$('#myModalButton').hide();
		$('#myModal').modal('show');
	});


	/* Pagination */
	$scope.prevPage = function () {
		if ($scope.currentPage > 0) {
			$scope.currentPage--;
		}
	};

	$scope.nextPage = function () {
		if ($scope.currentPage < $scope.getNumberOfPages()) {
			$scope.currentPage++;
		}
	};

	$scope.setPage = function () {
		$scope.currentPage = this.n;
	};

	$scope.getPagedQuestions = function () {
		var j = ($scope.questions.length-1);
		for (var i = 0; i < $scope.questions.length; i++) {
			if (i % $scope.pageSize === 0) {
				$scope.groupedQuestions[Math.floor(i / $scope.pageSize)] = [ $scope.questions[j] ];
			} else {
				$scope.groupedQuestions[Math.floor(i / $scope.pageSize)].push($scope.questions[j]);
			}
			j--;
		}
		return $scope.groupedQuestions[$scope.currentPage]
	};

	$scope.getNumberOfPages = function () {
		return Math.floor(($scope.questions.length - 1) / $scope.pageSize);
	}

	$scope.getDisplayedPages = function () {
		var displayedPages = [];

		// Display first 2 pages
		for(var i = 0; i < 2; i++) {
			if ($scope.currentPage > i) {
				displayedPages.push(i);
			}
		}

		// Display n previous pages
		for (var i = ($scope.currentPage - $scope.aroundPages); i < $scope.currentPage; i++) {
			if (i > 1) {
				displayedPages.push(i);
			}
		}

		// Display current page
		displayedPages.push($scope.currentPage);

		// Display n next pages
		for (var i = ($scope.currentPage); i < ($scope.currentPage + $scope.aroundPages); i++) {
			if (i < ($scope.getNumberOfPages() - 2)) {
				displayedPages.push(i+1);
			}
		}
					
		// Display last 2 pages
		for(var i = ($scope.getNumberOfPages()-1); i < $scope.getNumberOfPages()+1; i++) {
			if ($scope.currentPage < i) {
				displayedPages.push(i);
			}
		}

		return displayedPages;
	}
	/* /Pagination */


	$scope.deleteQuestion = function (id) {
		$scope.questions = Question.remove({id: id});
		$timeout(function() {
			$scope.questions = Question.query();
		}, 100);
		//$location.path('/questions');
	};

	/* "ShowMore" Pagination */
	// $scope.questionsLimit = function() {
	// 	return currentPage * pageSize;
	// };

	// $scope.showMoreQuestions = function () {
	// 	currentPage = currentPage + 1;
	// }

	// $scope.hasMoreQuestionsToShow = function () {
	// 	return currentPage < ($scope.questions.length / pageSize);
	// }
}
