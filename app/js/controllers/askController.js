'use strict';

/* Ask Controller */

function askCtrl($scope, $location, $timeout, $http, Question) {

	$scope.formInfo = {};
	$scope.formInfo.title = "";
	$scope.formInfo.text = "";

	$scope.newQuestion = {};
	$scope.newQuestion.title = "";
	$scope.newQuestion.text = "";

	$scope.tags = "";

	
	if($scope.logged === "true") {

	}
	else {
		$location.path('/login');
	}
	
	$scope.addQuestion = function() {
		$scope.formInfo.title = "";
		$scope.formInfo.text = "";

		if($scope.newQuestion === undefined || $scope.newQuestion.title === "" || $scope.newQuestion.text === "" || $scope.newQuestion.title === undefined || $scope.newQuestion.text === undefined) {
			if($scope.newQuestion.title === "" || $scope.newQuestion.title === undefined) {
				$scope.formInfo.title = "You must give a title to you question";
			}
			if($scope.newQuestion.text === "" || $scope.newQuestion.text === undefined) {
				$scope.formInfo.text = "You must describe your problem";
			}
		}
		else {
			var objectTags = [];

			if($scope.tags !== "") {
				var arrayTags = $scope.tags.split(";");

				for (var i = arrayTags.length - 1; i >= 0; i--) {
					objectTags.push({'tag' : arrayTags[i], 'description' : '', '_id' : ''});
				};
			}
			

			//$scope.newQuestion.tags = objectTags;
			console.log($scope.newQuestion);


			Question.save($scope.newQuestion,
				function(data) {
					$scope.newQuestion = '';
					$timeout(function() {
						$location.path('/questions');
					}, 200);
				},
				function(error) {
					console.log("Error "+error.status);
					$scope.getModal("error "+error.status, "")
				}
			);
		}
		
	};

}