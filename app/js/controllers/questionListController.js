'use strict';

/* Question List Controller */

function QuestionListCtrl($scope, $location, $filter, $timeout, Question) {

	$scope.questions = [];
	$scope.groupedQuestions = [];
	$scope.currentPage = 0;
	$scope.aroundPages = 3;

	Question.query(function(data) {
		console.info("Querying question list");
		$scope.questions = data;
	}, function(error) {
		console.warn("Error while querying question list");
		var titre = "";
		var text = "";
		console.warn("Error : "+error.status);
		switch(error.status) {
			case 401 : 
				titre = "Error 401";
				text = "Access denied";
				break;
			case 403 : 
				titre = "Error 403";
				text = "Server";
				break;
			default : 
				titre = "Error "+error.status;
				text = "Error";
				break;
		}

		$('#myModalLabel').html(titre);
		$('#myModalText').html(text);
		$('#myModalButton').hide();
		$('#myModal').modal('show');
	});


	/* Pagination */
	$scope.prevPage = function () {
		console.info("Previous page");
		if ($scope.currentPage > 0) {
			$scope.currentPage--;
		}
	};

	$scope.nextPage = function () {
		console.info("Next page");
		if ($scope.currentPage < $scope.getNumberOfPages()) {
			$scope.currentPage++;
		}
	};

	// $scope.setPage = function () {
	// 	$scope.currentPage = this.n;
	// };

	$scope.setPage = function (page) {
		console.info("Setting page "+page);
		$scope.currentPage = page;
	};

	$scope.getPagedQuestions = function () {
		console.info("Gathering questions by pages");
		var j = (($scope.questions.length)-1);
		for (var i = 0; i < $scope.questions.length; i++) {
			if (i % $scope.pageSize === 0) {
				$scope.groupedQuestions[Math.floor(i / $scope.pageSize)] = [ $scope.questions[j] ];
			} else {
				$scope.groupedQuestions[Math.floor(i / $scope.pageSize)].push($scope.questions[j]);
			}
			j--;
		}
		return $scope.groupedQuestions[$scope.currentPage]
	};

	$scope.getNumberOfPages = function () {
		return Math.floor(($scope.questions.length - 1) / $scope.pageSize);
	}

	// $scope.getDisplayedPages = function () {
	// 	var displayedPages = [];

	// 	// Display first 2 pages
	// 	for(var i = 0; i < 2; i++) {
	// 		if ($scope.currentPage > i) {
	// 			displayedPages.push(i);
	// 		}
	// 	}

	// 	// Display n previous pages
	// 	for (var i = ($scope.currentPage - $scope.aroundPages); i < $scope.currentPage; i++) {
	// 		if (i > 1) {
	// 			displayedPages.push(i);
	// 		}
	// 	}

	// 	// Display current page
	// 	displayedPages.push($scope.currentPage);

	// 	// Display n next pages
	// 	for (var i = ($scope.currentPage); i < ($scope.currentPage + $scope.aroundPages); i++) {
	// 		if (i < ($scope.getNumberOfPages() - 2)) {
	// 			displayedPages.push(i+1);
	// 		}
	// 	}
					
	// 	// Display last 2 pages
	// 	for(var i = ($scope.getNumberOfPages()-1); i < $scope.getNumberOfPages()+1; i++) {
	// 		if ($scope.currentPage < i) {
	// 			displayedPages.push(i);
	// 		}
	// 	}

	// 	return displayedPages;
	// }

	$scope.getDisplayedPages = function () {
		var displayedPages = [];
		var numberOfDisplayedPages = $scope.aroundPages * 2 + 1;
		var start = 0;

		if($scope.currentPage < $scope.aroundPages) {
			start = 0;
		}
		else if($scope.currentPage > $scope.getNumberOfPages() - $scope.aroundPages) {
			start = $scope.getNumberOfPages() - numberOfDisplayedPages + 1;
			if(start < 0) {
				start = 0
			}
		}
		else {
			start = $scope.currentPage - $scope.aroundPages;
		}

		for(var i = start; i < start + numberOfDisplayedPages-1; i++) {
			if(i <= $scope.getNumberOfPages()) {
				displayedPages.push(i);
			}
			else {
				break;
			}
		}

		return displayedPages;
	}

	/* /Pagination */

	/* "ShowMore" Pagination */
	// $scope.questionsLimit = function() {
	// 	return currentPage * pageSize;
	// };

	// $scope.showMoreQuestions = function () {
	// 	currentPage = currentPage + 1;
	// }

	// $scope.hasMoreQuestionsToShow = function () {
	// 	return currentPage < ($scope.questions.length / pageSize);
	// }
}
