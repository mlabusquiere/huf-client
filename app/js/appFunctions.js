'use strict';

/* App Module */

hufApp.run(function($rootScope, $timeout, $http) {

	$rootScope.pageSize = 5;
	$rootScope.username = "";

	$rootScope.getStatus = function (type, status) {
		//console.info("getStatus");

		var title = "";
		var text = "";

		switch(parseInt(status)) {

			case 400 : 
				title = "Error "+status+" : Bad Request";
				text = "Sorry, I don't speak Chinese";
				break;
			case 401 : 
				title = "Error "+status+" : Access denied";
				text = "Hey you ! What are you doing here ?!?";
				break;
			case 403 : 
				title = "Error "+status+" : Forbidden";
				text = "- Gandalf : \"You shall not pass !\"";
				break;
			case 404 : 
				title = "Error "+status+" : Not Found";
				text = "Houston we've got a problem, the moon has disappeared !";
				break;
			case 408 : 
				title = "Error "+status+" : Request Timeout";
				text = "Error "+status+" : Request Timeout";
				break;
			case 415 : 
				title = "Error "+status+" : Unsupported Media Type";
				text = "Error "+status+" : Request Timeout";
				break;

			case 500 : 
				title = "Error "+status+" : Internal Server Error";
				text = "Error "+status+" : Internal Server Error";
				break;
			case 502 : 
				title = "Error "+status+" : Bad Gateway";
				text = "Error "+status+" : Bad Gateway";
				break;
			case 503 : 
				title = "Error "+status+" : Service Unavailable";
				text = "Error "+status+" : Service Unavailable";
				break;

			default : 
				title = "Error "+status;
				text = "Error "+status;
				break;
		}

		switch(type) {
			case "title" : return title; break;
			case "text" : return text; break;
		}

		return null;
	};

	$rootScope.getModal = function (titre, text) {
		//console.info("getModal");
		$('#myModalLabel').html(titre);
		$('#myModalText').html(text);
		$('#myModalButton').hide();
		$('#myModal').modal('show');
	}


	$rootScope.logout = function () {
		console.info("Logout");
		$http.get('http://localhost:8080/huf-api/rest/logout').
		success(function(data, status, headers, config) {
			$rootScope.logged = false;
			console.info("Logged : "+$rootScope.logged);
			$('#myModalLabel').html("log out");
			$('#myModalText').html("Vous avez bien été déconnecté");
			$('#myModalButton').hide();
			$('#myModal').modal('show');
			setTimeout(function() {$('#myModal').modal('hide');}, 1300);
		}).
		error(function(data, status, headers, config){
			console.warn('Try again');
			console.warn("Logged : "+$rootScope.logged);
			$('#myModalLabel').html("Error");
			$('#myModalText').html("Il y a eu un problème lors de votre déconnection");
			$('#myModalButton').hide();
			$('#myModal').modal('show');
			setTimeout(function() {$('#myModal').modal('hide');}, 3000);
		});
	}

	$rootScope.isLogged = function () {
		console.info("isLogged");
		$http.get('http://localhost:8080/huf-api/rest/isauthenticated').
		success(function(data, status, headers, config) {
			$rootScope.logged = data;
			console.log("isLogged "+$rootScope.logged);
		}).
		error(function(data, status, headers, config){
			console.warn('Try again');
			$('#myModalLabel').html("Error");
			$('#myModalText').html("Problème");
			$('#myModalButton').hide();
			$('#myModal').modal('show');
			setTimeout(function() {$('#myModal').modal('hide');}, 3000);
		});
	}

	/* Pagination */
	// $rootScope.prevPage = function () {
	// 	if ($scope.currentPage > 0) {
	// 		$scope.currentPage--;
	// 	}
	// };

	// $rootScope.nextPage = function () {
	// 	if ($scope.currentPage < $scope.getNumberOfPages()) {
	// 		$scope.currentPage++;
	// 	}
	// };

	// $rootScope.setPage = function () {
	// 	$scope.currentPage = this.n;
	// };

	// $rootScope.getPagedQuestions = function () {
	// 	var j = ($scope.questions.length-1);
	// 	for (var i = 0; i < $scope.questions.length; i++) {
	// 		if (i % $scope.pageSize === 0) {
	// 			$scope.groupedQuestions[Math.floor(i / $scope.pageSize)] = [ $scope.questions[j] ];
	// 		} else {
	// 			$scope.groupedQuestions[Math.floor(i / $scope.pageSize)].push($scope.questions[j]);
	// 		}
	// 		j--;
	// 	}
	// 	return $scope.groupedQuestions[$scope.currentPage]
	// };

	// $rootScope.getNumberOfPages = function () {
	// 	return Math.floor(($scope.questions.length - 1) / $scope.pageSize);
	// }

	// $rootScope.getDisplayedPages = function () {
	// 	var displayedPages = [];

	// 	// Display first 2 pages
	// 	for(var i = 0; i < 2; i++) {
	// 		if ($scope.currentPage > i) {
	// 			displayedPages.push(i);
	// 		}
	// 	}

	// 	// Display n previous pages
	// 	for (var i = ($scope.currentPage - $scope.aroundPages); i < $scope.currentPage; i++) {
	// 		if (i > 1) {
	// 			displayedPages.push(i);
	// 		}
	// 	}

	// 	// Display current page
	// 	displayedPages.push($scope.currentPage);

	// 	// Display n next pages
	// 	for (var i = ($scope.currentPage); i < ($scope.currentPage + $scope.aroundPages); i++) {
	// 		if (i < ($scope.getNumberOfPages() - 2)) {
	// 			displayedPages.push(i+1);
	// 		}
	// 	}
					
	// 	// Display last 2 pages
	// 	for(var i = ($scope.getNumberOfPages()-1); i < $scope.getNumberOfPages()+1; i++) {
	// 		if ($scope.currentPage < i) {
	// 			displayedPages.push(i);
	// 		}
	// 	}

	// 	return displayedPages;
	// }
	/* /Pagination */

});
