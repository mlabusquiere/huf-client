Heap Underflow Web Client
=======================

HTML/AngularJs Client for Heap Underflow Q&A server application.

Checkout HUF server application, [Heap Underflow](https://bitbucket.org/mlabusquiere/heapunderflow2.0)


Usage
-----

Checkout the code:

    git clone git://...
    cd huf-client


Enjoy!



License
-------

Open Source
<!-- Source code released under an [MIT license](http://en.wikipedia.org/wiki/MIT_License) -->

Pull requests welcome.


Contributing
------------

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Added some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request


Authors
-------

* Soraya Bouakkaz
* Damien Deis
* Cécile Dispa
* Maxence Labusquière

En partenariat avec [Zenika](http://zenika.fr), suiveur : Olivier Gonthier



